﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zl.Model;

namespace Zl.IService
{
    public interface IOrganizeService : IBaseService<OrganizeModel>
    {
        IEnumerable<OrganizeModel> GetOrganizeList();
        IEnumerable<TreeSelect> GetOrganizeTreeSelect();
    }
}
